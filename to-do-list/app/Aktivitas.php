<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aktivitas extends Model
{
    protected $table = "aktivitas";
    public function catatan()
    {
        return $this->belongsToMany('App\Catatan', 'catatan_id', 'aktivitas_id');
    }
}
