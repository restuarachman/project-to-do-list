<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catatan extends Model
{
    protected $table = "catatan";
    public function todolist(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function aktivitas()
    {
        return $this->belongsToMany('App\Aktivitas', 'catatan_id', 'aktivitas_id');
    }
}
