<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aktivitas;

class AktivitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aktivitas = Aktivitas::all();
        return view('aktivitas.index', compact('aktivitas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $aktivitas = Aktivitas::all();
        return view('aktivitas.create', compact('aktivitas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        $request->validate([
            'kategori' => 'required|unique:aktivitas',
        ]);
        
        $aktivitas = new Aktivitas;
        $aktivitas->kategori = $request["kategori"];
        $aktivitas->save();

        return redirect('/aktivitas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aktivitas = Aktivitas::find($id);
        return view('', compact('aktivitas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aktivitas = Aktivitas::find($id);
        return view('aktivitas.edit', compact('aktivitas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'kategori' => 'required'
        // ]);
        $update = Aktivitas::where('id', $id)->update([
            "kategori" => $request["kategori"]
        ]);

        return redirect('/aktivitas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Aktivitas::destroy($id);
        return redirect('/aktivitas')->with('success', 'Aktivitas berhasil dihapus');

    }
}
