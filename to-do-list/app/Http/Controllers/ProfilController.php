<?php

namespace App\Http\Controllers;

use App\Profil;
use App\User;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $profil = Profil::all();

        return view('', compact('profil'));
        // return response()->json([
        //     "status" => 200,
        //     "massage" => "success",
        //     "data" => $profile
        // ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:profil',
            'usia' => 'required',
            'alamat' => 'required',
            'email' => 'required'
        ]);

        $profil = new Profil;
        $profil->nama = $request["nama"];
        $profil->usia = $request["usia"];
        $profil->alamat = $request["alamat"];
        $profil->email = $request["email"];
        $profil->save();

        return redirect('');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profil = User::find($id);
        $user = Profil::where('user_id', $profil->id)->first();
        return view('/profil/index', compact(['profil', 'user']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profil = Profil::find($id);
        return view('', compact('profil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'nama' => 'required',
        //     'usia' => 'required',
        //     'alamat' => 'required',
        //     'email' => 'required'
        // ]);
        $update = Profil::where('id', $id)->update([
            "nama" => $request["nama"],
            "usia" => $request["usia"],
            "alamat" => $request["alamat"],
            "email" => $request["email"]
        ]);
        
        return redirect('');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profil::destroy($id);
        return redirect('')->with('success', 'Profil berhasil dihapus');
        // return redirect('');
    }
}
