<?php

namespace App\Http\Controllers;

use App\Catatan;
use App\Aktivitas;
use App\Relasi;
use DB;
use Illuminate\Http\Request;

class CatatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $catatan = Catatan::all();
        $kat = DB::table('relasi')
            ->join('aktivitas', 'relasi.aktivitas_id', '=', 'aktivitas.id')
            ->join('catatan', 'relasi.catatan_id', '=', 'catatan.id')
            ->select('aktivitas.kategori')
            ->get();
    
        return view('catatan.index', compact('catatan', 'kat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Aktivitas::all();
        return view('catatan.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        $request->validate([
            'judul' => 'required|unique:catatan',
            'isi' => 'required',
        ]);

        $catatan = new Catatan;
        $catatan->user_id = 1;
        $catatan->judul = $request["judul"];
        $catatan->isi = $request["isi"];
        $catatan->save();

        $aktivitas = new Aktivitas;
        $aktivitas = Aktivitas::where('kategori', $request->kategori)->get();
        $cat =  Catatan::where('judul', $request->judul)->get();

        $relasi = new Relasi;
        $relasi->catatan_id = $cat[0]->id;
        $relasi->aktivitas_id = $aktivitas[0]->id;
        $relasi->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $catatan = Catatan::find($id);
        return view('', compact('catatan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catatan = Catatan::find($id);
        $relasi = Relasi::where('catatan_id', $id)->get();
        $current_kategori = Aktivitas::where('id', $relasi[0]->aktivitas_id)->get();
        $kategori = Aktivitas::all();
        //dd($current_kategori[0]->kategori);
        return view('catatan.edit', compact('catatan', 'current_kategori', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'judul' => 'required',
        //     'isi' => 'required',
        // ]);
        $update = Catatan::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
        ]);

        $aktivitas = Aktivitas::where('kategori', $request["kategori"])->get();

        $updaterelasi = Relasi::where('catatan_id', $id)->update([
            "aktivitas_id" => $aktivitas[0]->id,
        ]);
        
        return redirect('/catatan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Catatan::destroy($id);
        return redirect('/catatan')->with('success', 'Catatan berhasil dihapus');
        // return redirect('');
    }
}
