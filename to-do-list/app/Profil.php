<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    // public $table = "profil";
    protected $table = "profil";
    protected $fillable = ['usia', 'alamat', 'users_id'];
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
