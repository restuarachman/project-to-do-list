<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('catatan_id');
            $table->foreign('catatan_id')->references('id')->on('catatan')->onDelete('cascade');
            $table->unsignedBigInteger('aktivitas_id');
            $table->foreign('aktivitas_id')->references('id')->on('aktivitas')->onDelete('cascade');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi');
    }
}
