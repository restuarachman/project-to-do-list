<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Aktivitas;

class CreateAktivitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aktivitas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kategori');
            $table->timestamps();
        });

        $act = new Aktivitas;
        $act->kategori = "Olahraga";
        $act->save();
        $act = new Aktivitas;
        $act->kategori = "Proyek";
        $act->save();
        $act = new Aktivitas;
        $act->kategori = "Kuliah";
        $act->save();
        $act = new Aktivitas;
        $act->kategori = "Meeting";
        $act->save();
        $act = new Aktivitas;
        $act->kategori = "Keluarga";
        $act->save();
        $act = new Aktivitas;
        $act->kategori = "Liburan";
        $act->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aktivitas');
    }
}
