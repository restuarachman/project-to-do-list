@extends('master')
@section('create')
    active
@endsection
@section('content')
<form role="form" action="/catatan" method="POST">
    @csrf
    <div class="mb-3">
        <label class="form-label">Judul</label>
        <input name="judul" type="text" class="form-control" placeholder="My Activity" value={{old("judul","")}}>
    </div>
    <div class="mb-3">
        <label class="form-label">Catatan</label>
        <textarea name="isi" class="form-control" rows="3" placeholder="...">{{old("isi","")}}</textarea>
    </div>

    <div class="mb-3">
        <label class="form-label">Kategori</label>
        <select name="kategori" class="form-select" aria-label="Default select example">
            <option selected>Open this select menu</option>
            @foreach($kategori as $key => $ktgri)
            <option value="{{$ktgri->kategori}}">{{$ktgri->kategori}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>  

@endsection