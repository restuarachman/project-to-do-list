@extends('master')
@section('dashboard')
    active
@endsection
@section('content')
<div class="row">
    @foreach($catatan as $key => $catat)
    <div class="col-sm-3">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">{{$catat->judul}}</h5>
                <p class="card-text">{{$catat->isi}}</p>
                <p class="card-text">{{$kat[$key]->kategori}}</p>
                <form action="catatan/{{$catat->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        <a href="/catatan/{{$catat->id}}/edit" class="btn btn-primary"><i class="bx bx-edit"></i> Edit</a>
                        <button type="submit" class="btn btn-danger"><i class="bx bx-trash"></i>Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection