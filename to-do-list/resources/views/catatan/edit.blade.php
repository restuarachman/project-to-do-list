@extends('master')
@section('create')
    active
@endsection
@section('content')
<form role="form" action="/catatan/{{$catatan->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="mb-3">
        <label class="form-label">Judul</label>
        <input name="judul" type="text" class="form-control" placeholder="My Activity" value="{{$catatan->judul}}">
    </div>
    <div class="mb-3">
        <label class="form-label">Catatan</label>
        <textarea name="isi" class="form-control" rows="3" placeholder="...">{{$catatan->isi}}</textarea>
    </div>
    <div class="mb-3">
        <label class="form-label">Kategori</label>
        <select name="kategori" class="form-select" aria-label="Default select example">
            <option selected>{{$current_kategori[0]->kategori}}</option>    
            @foreach($kategori as $key => $ktgri)
            <option value="{{$ktgri->kategori}}">{{$ktgri->kategori}}</option>
            @endforeach
        </select>
    </div>
   
    <div class="mt-4">
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>  

@endsection