@extends('master')
@section('dashboard')
    active
@endsection
@section('content')
<div class="row">
    {{dd($profil)}}
    @foreach($profil as $val)
    <div class="col-sm-3">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <p class="card-title">{{$val->profil->name}}</p>
                <p class="card-text">{{$val->profil->email}}</p>
                <p class="card-text">{{$val->usia}}</p>
                <p class="card-text">{{$val->alamat}}</p>
                
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection