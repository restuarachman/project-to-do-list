<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>To Do List</title>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <link href='{{asset('/css/sidebar.css')}}' rel='stylesheet'>
</head>
<body oncontextmenu='return false' class='snippet-body body-pd'>
    <body id="body-pd" class="body-pd">
        <header class="header body-pd" id="header">
            <div class="header_toggle"> 
               <p>MyList</p>
            </div>
            <!-- <div class="header_img"> 
                <img src="{{asset('/img/img.png')}}" alt="">
                
            </div> -->
            <div>
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            
                           <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('profil/'. Auth::user()->id)}}">
                                    Profil
                                </a>
                                
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
            
        </header>
        <!-- SideBar -->
        <div class="l-navbar show" id="nav-bar">
            <nav class="nav">
                <div> 
                    <a href="/catatan" class="nav_logo"> 
                        <i class='bx bx-layer nav_logo-icon'></i> 
                        <span class="nav_logo-name">BBBootstrap</span> 
                    </a>
                    <div class="nav_list"> 
                        <a href="/catatan" class="nav_link @yield('dashboard')" id="dashboard"> 
                            <i class='bx bx-grid-alt nav_icon'></i> 
                            <span class="nav_name">Dashboard</span> 
                        </a> 
                        <a href="#" class="nav_link @yield('settings')" id="settings"> 
<<<<<<< HEAD
                            <i class="bx bx-cog nav_icon"></i>
                            <span class="nav_name">Settings</span> 
                        </a> 
                        <a href="#" class="nav_link @yield('activity')" id="all-activity"> 
=======
                            <i class="bx bx-user nav_icon"></i>
                            <span class="nav_name">Profil</span> 
                        </a>  
                        <a href="/aktivitas" class="nav_link @yield('kategori')" id="new-project"> 
>>>>>>> 614075451a0caf559f76aaeb7849a6bbda2ef613
                            <i class='bx bx-message-square-detail nav_icon'></i> 
                            <span class="nav_name">Kategori </span> 
                        </a>
                    </div>
                </div> 
                <div>
                    <a href="/catatan/create" class="nav_link @yield('create')" id="new-project"> 
                        <i class='bx bx-plus nav_icon'></i> 
                        <span class="nav_name">List Baru</span> 
                    </a>
                </div>
                
            </nav>
        </div>
        
        <div class="container">
           @yield('content')
        </div>
        <!--Container Main end-->
        <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>
        <script type='text/javascript' src="{{asset('/js/sidebar.js')}}"></script>
    </body>
</body>
</html>