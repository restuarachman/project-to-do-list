@extends('master')
@section('kategori')
    active
@endsection
@section('content')
<form role="form" action="/aktivitas/{{$aktivitas->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="mb-3">
        <label class="form-label">Nama Kategori</label>
        <input name="kategori" type="text" class="form-control" placeholder="My Activity" value="{{$aktivitas->kategori}}">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>  
@endsection