@extends('master')
@section('kategori')
    active
@endsection
@section('content')
<div class="d-flex justify-content-between">
    <h4>Table Kategori</h4>
    <a href="/aktivitas/create" class="btn btn-success"> <i class="bx bx-plus"></i>Add</a>
</div>

<table class="table">
  <thead>
    <tr>
        <th scope="col" style="width:10%">ID</th>
        <th scope="col" style="width:75%">Nama Kategori</th>
        <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($aktivitas as $key => $kategori)
    <tr>
        <th scope="row">{{$kategori->id}}</th>
        <td>{{$kategori->kategori}}</td>
        <td>
            <form action="aktivitas/{{$kategori->id}}" method="POST">
                @csrf
                @method('DELETE')
                <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                    <a href="/aktivitas/{{$kategori->id}}/edit" class="btn btn-primary">
                        <i class="bx bx-edit"></i> 
                        Edit
                    </a>
                    <button type="submit" class="btn btn-danger">
                        <i class="bx bx-trash"></i>
                        Delete
                    </button>
                </div>
            </form>
        </td>
    </tr>
    @endforeach
</table>
@endsection