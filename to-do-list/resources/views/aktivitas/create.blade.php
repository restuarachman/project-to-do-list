@extends('master')
@section('kategori')
    active
@endsection
@section('content')
<form role="form" action="/aktivitas" method="POST">
    @csrf
    <div class="mb-3">
        <label class="form-label">Nama Kategori</label>
        <input name="kategori" type="text" class="form-control" placeholder="My Activity" value={{old("judul","")}}>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>  

@endsection